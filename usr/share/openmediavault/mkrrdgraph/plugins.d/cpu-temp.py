# -*- coding: utf-8 -*-
#
import openmediavault.mkrrdgraph
import openmediavault.subprocess

class Plugin(openmediavault.mkrrdgraph.IPlugin):
	def create_graph(self, config):
		# http://paletton.com/#uid=33r0-0kwi++bu++hX++++rd++kX
		config.update({
			'title_load': 'CPU temperature',
			'COLOR_CANVAS':'#ffffff',
			'COLOR_CPUTEMP':'#ff0000',
			'COLOR_CPUTEMP_TRANS':'#ff000044'
		})
		args = []
		args.append('{image_dir}/cputemp-{period}.png'.format(**config))
		args.extend(config['defaults'])
		args.extend(['--start', config['start']])
		args.extend(['--title', '"{title_load}{title_by_period}"'.format(**config)])
		args.append('--slope-mode')
		args.extend(['--lower-limit', '20'])
		args.extend(['--vertical-label', '°Celsius'])
		args.append('DEF:cputemp_avg={data_dir}/temperature/temperature-cpu.rrd:value:AVERAGE'.format(**config))
		args.append('DEF:cputemp_min={data_dir}/temperature/temperature-cpu.rrd:value:MIN'.format(**config))
		args.append('DEF:cputemp_max={data_dir}/temperature/temperature-cpu.rrd:value:MAX'.format(**config))
		args.append('AREA:cputemp_max{COLOR_CPUTEMP_TRANS}'.format(**config))
		args.append('AREA:cputemp_min{COLOR_CANVAS}'.format(**config))
		args.append('LINE1:cputemp_avg{COLOR_CPUTEMP}:"Temperature" '.format(**config))
		args.append('GPRINT:cputemp_min:MIN:"%5.1lf%s Min"')
		args.append('GPRINT:cputemp_avg:AVERAGE:"%5.1lf%s Avg"')
		args.append('GPRINT:cputemp_max:MAX:"%5.1lf%s Max"')
		args.append('GPRINT:cputemp_avg:LAST:"%5.1lf%s Last\l"')
		args.append('COMMENT:"{last_update}"'.format(**config))
		openmediavault.mkrrdgraph.call_rrdtool_graph(args)
		return 0
