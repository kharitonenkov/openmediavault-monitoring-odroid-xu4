# -*- coding: utf-8 -*-
#
import openmediavault.mkrrdgraph
import openmediavault.subprocess

class Plugin(openmediavault.mkrrdgraph.IPlugin):
	def create_graph(self, config):
		# http://paletton.com/#uid=33r0-0kwi++bu++hX++++rd++kX
		config.update({
			'title_load': 'CPU frequency',
			'COLOR_CANVAS':'#ffffff',
			'COLOR_CPUFREQ_A7':'#0000fd',
			'COLOR_CPUFREQ_A15':'#ff0000'
		})
		args = []
		args.append('{image_dir}/cpufreq-{period}.png'.format(**config))
		args.extend(config['defaults'])
		args.extend(['--start', config['start']])
		args.extend(['--title', '"{title_load}{title_by_period}"'.format(**config)])
		args.append('--slope-mode')
		args.extend(['--lower-limit', '0'])
		args.extend(['--vertical-label', 'Hz'])
		args.append('DEF:cpufreq0_avg={data_dir}/cpufreq/cpufreq-0.rrd:value:AVERAGE'.format(**config))
		args.append('DEF:cpufreq0_min={data_dir}/cpufreq/cpufreq-0.rrd:value:MIN'.format(**config))
		args.append('DEF:cpufreq0_max={data_dir}/cpufreq/cpufreq-0.rrd:value:MAX'.format(**config))
		args.append('DEF:cpufreq4_avg={data_dir}/cpufreq/cpufreq-4.rrd:value:AVERAGE'.format(**config))
		args.append('DEF:cpufreq4_min={data_dir}/cpufreq/cpufreq-4.rrd:value:MIN'.format(**config))
		args.append('DEF:cpufreq4_max={data_dir}/cpufreq/cpufreq-4.rrd:value:MAX'.format(**config))
		args.append('LINE1:cpufreq0_avg{COLOR_CPUFREQ_A7}:" Cortex-A7"'.format(**config))
		args.append('GPRINT:cpufreq0_min:MIN:"%5.1lf%s Min"')
		args.append('GPRINT:cpufreq0_avg:AVERAGE:"%5.1lf%s Avg" ')
		args.append('GPRINT:cpufreq0_max:MAX:"%5.1lf%s Max"')
		args.append('GPRINT:cpufreq0_avg:LAST:"%5.1lf%s Last\l"')
		args.append('LINE1:cpufreq4_avg{COLOR_CPUFREQ_A15}:"Cortex-A15"'.format(**config))
		args.append('GPRINT:cpufreq4_min:MIN:"%5.1lf%s Min"')
		args.append('GPRINT:cpufreq4_avg:AVERAGE:"%5.1lf%s Avg" ')
		args.append('GPRINT:cpufreq4_max:MAX:"%5.1lf%s Max"')
		args.append('GPRINT:cpufreq4_avg:LAST:"%5.1lf%s Last\l"')
		args.append('COMMENT:"{last_update}"'.format(**config))
		openmediavault.mkrrdgraph.call_rrdtool_graph(args)
		return 0
