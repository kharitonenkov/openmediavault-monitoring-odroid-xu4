# -*- coding: utf-8 -*-
#
import openmediavault.mkrrdgraph
import openmediavault.subprocess

class Plugin(openmediavault.mkrrdgraph.IPlugin):
	def create_graph(self, config):
		# http://paletton.com/#uid=33r0-0kwi++bu++hX++++rd++kX
		config.update({
			'title_load': 'CPU fan speed',
			'COLOR_CANVAS':'#ffffff',
			'COLOR_CPUFANSPEED':'#ff0000',
			'COLOR_CPUFANSPEED_TRANS':'#ff000044'
		})
		args = []
		args.append('{image_dir}/cpufanspeed-{period}.png'.format(**config))
		args.extend(config['defaults'])
		args.extend(['--start', config['start']])
		args.extend(['--title', '"{title_load}{title_by_period}"'.format(**config)])
		args.append('--slope-mode')
		args.extend(['--upper-limit', '100'])
		args.extend(['--lower-limit', '0'])
		args.append('--rigid')
		args.extend(['--vertical-label', 'Percent'])
		args.append('DEF:cpufanspeed_avg={data_dir}/fanspeed/fanspeed.rrd:value:AVERAGE'.format(**config))
		args.append('DEF:cpufanspeed_min={data_dir}/fanspeed/fanspeed.rrd:value:MIN'.format(**config))
		args.append('DEF:cpufanspeed_max={data_dir}/fanspeed/fanspeed.rrd:value:MAX'.format(**config))
		args.append('AREA:cpufanspeed_max{COLOR_CPUFANSPEED_TRANS}'.format(**config))
		args.append('AREA:cpufanspeed_min{COLOR_CANVAS}'.format(**config))
		args.append('LINE1:cpufanspeed_avg{COLOR_CPUFANSPEED}:"Speed"'.format(**config))
		args.append('GPRINT:cpufanspeed_min:MIN:"%5.1lf%s Min"')
		args.append('GPRINT:cpufanspeed_avg:AVERAGE:"%5.1lf%s Avg"')
		args.append('GPRINT:cpufanspeed_max:MAX:"%5.1lf%s Max"')
		args.append('GPRINT:cpufanspeed_avg:LAST:"%5.1lf%s Last\l"')
		args.append('COMMENT:"{last_update}"'.format(**config))
		openmediavault.mkrrdgraph.call_rrdtool_graph(args)
		return 0
